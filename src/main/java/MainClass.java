import Controller.ComandosController;
import Model.Elements.Elemento;
import Model.SistemaTree.Node;
import Model.SistemaTree.Tree;
import Utils.PermissoesEnum;

import java.util.Scanner;

public class MainClass {
    public static void main(String[] args) {
        Elemento elementoRoot = new Elemento("root", "root");
        Node root = new Node(elementoRoot);
        Tree tree = new Tree(root);
        Node currentNode = root;
        String res;
        PermissoesEnum userPermission = PermissoesEnum.OPEN;

        System.out.println("||| WELCOME TO THE FILE SYSTEM |||");
        System.out.println("||| 'help' for commands; 'exit' to leave |||\n");
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.print("~" + currentNode.getData().getPath() + " : ");
            res = scanner.nextLine();

            ComandosController controller = new ComandosController(res, currentNode, tree, userPermission);

            String[] resStrings = res.split(" ");

            switch (resStrings[0].toLowerCase().trim()) {
                case "mkdir":
                    System.out.println(controller.criarPasta());
                    break;
                case "touch":
                    System.out.println(controller.criarFicheiro());
                    break;
                case "ls":
                    System.out.println(controller.obterElementos());
                    break;
                case "cd":
                    currentNode = controller.modificarPathAtual();
                    System.out.println("\n- moved to " + currentNode.getData().getPath() + "\n");
                    break;
                case "mv":
                    System.out.println(controller.moverElemento());
                    break;
                case "rm":
                    System.out.println(controller.removerElemento());
                    break;
                case "cp":
                    System.out.println(controller.copiarElemento());
                    break;
                case "cat":
                    System.out.println(controller.abrirFicheiro());
                    break;
                case "edit":
                    System.out.println(controller.editarFicheiro());
                    break;
                case "sudo":
                    userPermission = PermissoesEnum.ADMIN_ONLY;
                    System.out.println("\n- changed to Admin \n");
                    break;
                case "unsudo":
                    userPermission = PermissoesEnum.OPEN;
                    System.out.println("\n- changed to Normal User \n");
                    break;
                case "help":
                    System.out.println("\n- Create pasta : mkdir *name*" +
                            "\n- Create file : touch *name* *?permissions?*" +
                            "\n- List folders and files on the current path : ls" +
                            "\n- Change path : cd *path*" +
                            "\n- Remove element from the system : rm *path*" +
                            "\n- Move element : mv *original path* *destination path*" +
                            "\n- Copy element : cp *original path* *destination path*" +
                            "\n- Edit a file : edit *file path*" +
                            "\n- Open a file : cat *file path*" +
                            "\n- Change to Admin : sudo" +
                            "\n- Change to normal user : unsudo \n");
                    break;
                case "exit":
                    break;
                default:
                    System.out.println("\n- Command Doesnt exist\n");
                    break;
            }

        } while (!res.equalsIgnoreCase("exit"));
        System.out.println("||| GOODBYE |||");

    }
}
