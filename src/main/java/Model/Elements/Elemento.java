package Model.Elements;

import Utils.PermissoesEnum;

import java.util.Objects;

public class Elemento {
    private String nome;
    private String path;
    private String dataCriacao;
    private String corpo;
    private PermissoesEnum permissoes;

    public Elemento(String nome, String path) {
        this.nome = nome;
        this.path = path;
        this.permissoes = PermissoesEnum.OPEN;
        corpo = "empty";
        dataCriacao = new Utils.Utils().currentDateGoodFormat();
    }

    public Elemento(String nome, String path, PermissoesEnum permissoes) {
        this.nome = nome;
        this.path = path;
        this.permissoes = permissoes;
        corpo = "empty";
        dataCriacao = new Utils.Utils().currentDateGoodFormat();
    }

    public String getNome() {
        return nome;
    }

    public String getPath() {
        return path;
    }

    public String getDataCriacao() {
        return dataCriacao;
    }

    public PermissoesEnum getPermissoes() {
        return permissoes;
    }

    public String getCorpo() {
        return corpo;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setCorpo(String corpo) {
        this.corpo = corpo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Elemento)) return false;
        Elemento elemento = (Elemento) o;
        return Objects.equals(nome, elemento.nome) &&
                Objects.equals(path, elemento.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nome, path);
    }
}
