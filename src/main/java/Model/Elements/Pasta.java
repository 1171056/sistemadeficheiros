package Model.Elements;

import Utils.PermissoesEnum;

public class Pasta extends Elemento {

    public Pasta(String nome, String path) {
        super(nome, path);
    }

    public Pasta(String nome, String path, PermissoesEnum permissao) {
        super(nome, path, permissao);
    }

    public String getNome() {
        return super.getNome();
    }

    public void setNome(String titulo) {
        super.setNome(titulo);
    }

}
