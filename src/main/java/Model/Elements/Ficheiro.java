package Model.Elements;

import Utils.PermissoesEnum;

public class Ficheiro extends Elemento {

    public Ficheiro(String nome, String path) {
        super(nome, path);
    }

    public Ficheiro(String nome, String path, PermissoesEnum permissao) {
        super(nome, path, permissao);
    }

    public String getNome() {
        return super.getNome();
    }

    public void setNome(String nome) {
        super.setNome(nome);
    }

}
