package Model.SistemaTree;

import Model.Elements.Elemento;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Node {

    private List<Node> children = new ArrayList<>();
    private Node parent = null;
    private Elemento data;

    public Node(Elemento data) {
        this.data = data;
    }

    public Node(Elemento data, Node parent) {
        this.data = data;
        this.parent = parent;
    }

    public List<Node> getChildren() {
        return children;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public void addChild(Elemento data) {
        Node child = new Node(data);
        child.setParent(this);
        this.children.add(child);
    }

    public void addChild(Node child) {
        child.setParent(this);
        this.children.add(child);
    }

    public void removeChild(Node child) {
        children.remove(child);
    }

    public Elemento getData() {
        return this.data;
    }

    public void setData(Elemento data) {
        this.data = data;
    }

    public boolean isRoot() {
        return (this.parent == null);
    }

    public boolean isLeaf() {
        return this.children.size() == 0;
    }

    public void removeParent() {
        this.parent = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Node)) return false;
        Node node = (Node) o;
        return Objects.equals(data.getPath(), node.data.getPath());
    }

    @Override
    public int hashCode() {
        return Objects.hash(children, parent, data);
    }
}
