package Model.SistemaTree;

import Model.Elements.Ficheiro;
import Model.Elements.Pasta;

import java.util.ArrayList;
import java.util.List;

public class Tree {
    private List<Node> elements;
    private Node root;

    public Tree(Node root) {
        this.root = root;
        this.elements = new ArrayList<>();
    }

    public Tree(List<Node> elements, Node root) {
        this.elements = elements;
        this.root = root;
    }

    public boolean isEmpty() {
        return root == null;
    }

    public List<Node> getElements() {
        return elements;
    }

    public void addElement(Node element) {
        this.elements.add(element);
    }

    public void removeElement(Node element) {
        Node parentNode = element.getParent();
        parentNode.removeChild(element);
        element.removeParent();
        if (element.getData().getClass().equals(Pasta.class)) {
            this.getElements().remove(element);
        }
    }

    public Node getRoot() {
        return root;
    }

    /**
     * testa se um path existe logo se existe um elemento criado com aquele path especifico
     *
     * @param path path a ser testado
     * @return boolean : true se existir; false se nao existir
     */
    public boolean doesFolderPathExist(String path) {
        for (Node element : this.getElements()) {
            if (element.getData().getPath().equalsIgnoreCase(path) && element.getData() instanceof Pasta) {
                return true;
            }
        }
        return false;
    }

    /**
     * testa se um path existe logo se existe um elemento criado com aquele path especifico
     *
     * @param path path a ser testado
     * @return boolean : true se existir; false se nao existir
     */
    public boolean doesFilePathExist(String path) {
        for (Node element : this.getElements()) {
            if (element.getData().getPath().equalsIgnoreCase(path) && element.getData() instanceof Ficheiro) {
                return true;
            }
        }
        return false;
    }


    /**
     * testa se um path existe logo se existe um elemento criado com aquele path especifico
     *
     * @param path path a ser testado
     * @return boolean : true se existir; false se nao existir
     */
    public boolean doesPathExist(String path) {
        for (Node element : this.getElements()) {
            if (element.getData().getPath().equalsIgnoreCase(path)) {
                return true;
            }
        }
        return false;
    }

}
