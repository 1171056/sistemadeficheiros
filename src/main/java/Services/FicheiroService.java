package Services;

import Model.Elements.Ficheiro;
import Model.Elements.Pasta;
import Model.SistemaTree.Node;
import Model.SistemaTree.Tree;
import Utils.*;

import java.util.Scanner;

public class FicheiroService {

    private Node currentNode;
    private Utils utils;
    private Tree tree;


    /**
     * Controller principal para as funcionalidades dos Comandos dos ficheiros
     *
     * @param currentNode elemento root da arvore do sistema
     * @param tree        arvore do sistema
     */
    public FicheiroService(Node currentNode, Tree tree) {
        this.currentNode = currentNode;
        this.tree = tree;
        utils = new Utils();
    }

    /**
     * Cria uma ficheiro atraves do nome dado pelo utilizador
     *
     * @return feedback do sucesso/insucesso da operacao
     */
    public boolean criarFicheiro(String path, String[] inputs) {
        if (inputs.length == 2) {
            Ficheiro ficheiro = new Ficheiro(inputs[1].trim(), path);
            Node ficheiroNode = new Node(ficheiro, currentNode);
            currentNode.addChild(ficheiroNode);
            tree.addElement(ficheiroNode);
            return true;
        } else if (inputs.length == 3) {
            Ficheiro ficheiro = new Ficheiro(inputs[1].trim(), path, PermissoesEnum.valueOf(inputs[2]));
            Node ficheiroNode = new Node(ficheiro, currentNode);
            currentNode.addChild(ficheiroNode);
            tree.addElement(ficheiroNode);
            return true;
        }
        return false;
    }

    /**
     * Abre o ficheiro para ser editado
     *
     * @return true se foi editado; false se ocorreu um erro
     */
    public boolean editarFicheiro(String[] inputs, PermissoesEnum userPermission) {
        if (inputs.length == 2) {
            Node ficheiroNode = null;
            for (Node child : currentNode.getChildren()) {
                if (child.getData().getPath().equalsIgnoreCase(inputs[1]) && child.getData() instanceof Ficheiro) {
                    ficheiroNode = child;
                }
            }
            if (ficheiroNode == null) {
                return false;
            }
            if (ficheiroNode.getData().getPermissoes().equals(PermissoesEnum.ADMIN_ONLY) && !ficheiroNode.getData().getPermissoes().equals(userPermission)) {
                return false;
            }
            System.out.println("\n||| Write phrases to add to the body of the file ('quit' to save and leave) |||\n");
            Scanner scanner = new Scanner(System.in);
            String res = "";
            StringBuilder body = new StringBuilder();
            do {
                body.append(res).append("\n");
                res = scanner.nextLine();
            } while (!res.equalsIgnoreCase("quit"));
            ficheiroNode.getData().setCorpo(body.toString());
            return true;
        }
        return false;
    }

    /**
     * Abre o ficheiro e mostra o corpo do ficheiro
     *
     * @return corpo do ficheiro
     */
    public String abrirFicheiro(String[] inputs) {
        if (inputs.length == 2) {
            for (Node child : currentNode.getChildren()) {
                if (child.getData().getPath().equalsIgnoreCase(inputs[1]) && child.getData() instanceof Ficheiro) {
                    return child.getData().getCorpo();
                }
            }
        }
        return "\n- ERROR: something went wrong or incorrect command\n";
    }
}
