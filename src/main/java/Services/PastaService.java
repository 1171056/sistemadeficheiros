package Services;

import Model.Elements.Pasta;
import Model.SistemaTree.Node;
import Model.SistemaTree.Tree;
import Utils.PermissoesEnum;
import Utils.Utils;

public class PastaService {

    private Node currentNode;
    private Tree tree;
    private Utils utils;

    /**
     * Controller principal para as funcionalidades dos Comandos das pastas
     *
     * @param currentNode elemento root da arvore do sistema
     * @param tree        arvore do sistema
     */
    public PastaService(Node currentNode, Tree tree) {
        this.currentNode = currentNode;
        this.tree = tree;
        utils = new Utils();
    }

    /**
     * Cria uma pasta atraves do nome dado pelo utilizador
     *
     * @return feedback do sucesso/insucesso da operacao
     */
    public boolean criarPasta(String path, String[] inputs) {
        if (inputs.length == 2) {
            Pasta pasta = new Pasta(inputs[1].trim(), path);
            Node pastaNode = new Node(pasta, currentNode);
            currentNode.addChild(pastaNode);
            tree.addElement(pastaNode);
            return true;
        }
        if (inputs.length == 3) {
            Pasta pasta = new Pasta(inputs[1].trim(), path, PermissoesEnum.valueOf(inputs[2]));
            Node pastaNode = new Node(pasta, currentNode);
            currentNode.addChild(pastaNode);
            tree.addElement(pastaNode);
            return true;
        }
        return false;
    }
}
