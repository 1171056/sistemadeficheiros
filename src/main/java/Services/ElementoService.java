package Services;

import Model.Elements.Elemento;
import Model.Elements.Pasta;
import Model.SistemaTree.Node;
import Model.SistemaTree.Tree;
import Utils.Utils;

public class ElementoService {

    private Node currentNode;
    private Tree tree;
    private Utils utils;

    /**
     * Controller principal para as funcionalidades dos Comandos dos elementos
     *
     * @param root elemento root da arvore do sistema
     * @param tree arvore do sistema
     */
    public ElementoService(Node root, Tree tree) {
        this.currentNode = root;
        this.tree = tree;
        utils = new Utils();
    }

    /**
     * Obtem todas as pastas e ficheiros criados
     *
     * @return feedback do sucesso/insucesso da operacao
     */
    public String obterElementos(String[] inputs) {
        if (inputs.length == 1) {
            return utils.lsFormatter(currentNode);
        }
        return "\n- ERROR: something went wrong or incorrect command\n";
    }

    /**
     * Modifica o path em que o utilizador se encontra
     *
     * @return path para o qual o user foi movido
     */
    public Node modificarPathAtual(String[] inputs) {
        if (inputs.length == 2) {
            if (inputs[1].equalsIgnoreCase("..") && !currentNode.isRoot()) {
                return currentNode.getParent();
            } else if (inputs[1].equalsIgnoreCase("/")) {
                return tree.getRoot();
            } else {
                for (Node element : tree.getElements()) {
                    if (element.getData().getPath().equalsIgnoreCase(inputs[1]) && element.getData() instanceof Pasta) {
                        return element;
                    }
                }
            }
        }
        return currentNode;
    }

    /**
     * Copia um ficheiro/pasta para a pasta pretendida
     *
     * @return feedback do sucesso/insucesso da operacao
     */
    public boolean copiarElemento(String[] inputs) {
        for (Node firstPathElement : tree.getElements()) {
            if (firstPathElement.equals(new Node(new Elemento("", inputs[1])))) {
                for (Node secondPathElement : tree.getElements()) {
                    if (secondPathElement.equals(new Node(new Elemento("", inputs[2]))) && secondPathElement.getData() instanceof Pasta) {
                        Pasta pasta = new Pasta(firstPathElement.getData().getNome(), inputs[2].trim() + "/" + firstPathElement.getData().getNome());
                        Node pastaNode = new Node(pasta, secondPathElement);
                        pastaNode = firstPathElement;
                        secondPathElement.addChild(pastaNode);
                        tree.addElement(pastaNode);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Mover um ficheiro/pasta para a pasta pretendida
     *
     * @return feedback do sucesso/insucesso da operacao
     */
    public boolean moverElemento(String[] inputs) {
        for (Node firstPathElement : tree.getElements()) {
            if (firstPathElement.equals(new Node(new Elemento("", inputs[1])))) {
                for (Node secondPathElement : tree.getElements()) {
                    if (secondPathElement.equals(new Node(new Elemento("", inputs[2]))) && secondPathElement.getData() instanceof Pasta) {
                        tree.removeElement(firstPathElement);
                        firstPathElement.getData().setPath(inputs[2].trim() + "/" + firstPathElement.getData().getNome());
                        secondPathElement.addChild(firstPathElement);
                        tree.addElement(firstPathElement);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Copia um ficheiro/pasta do sistema
     *
     * @return feedback do sucesso/insucesso da operacao
     */
    public boolean removerElemento(String[] inputs) {
        if (inputs.length == 2) {
            if (!tree.doesPathExist(inputs[1])) {
                return false;
            }
            for (Node element : tree.getElements()) {
                if (element.equals(new Node(new Elemento("", inputs[1])))) {
                    tree.removeElement(element);
                    return true;
                }
            }
        }
        return false;
    }

}
