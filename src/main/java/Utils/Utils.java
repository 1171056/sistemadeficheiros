package Utils;

import Model.Elements.Ficheiro;
import Model.Elements.Pasta;
import Model.SistemaTree.Node;
import Model.SistemaTree.Tree;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    public Utils() {
    }

    /**
     * formata a apresentacao dos filhos de um certo node
     *
     * @param node node ao qual se vai buscar os filhos
     * @return string formatado
     */
    public String lsFormatter(Node node) {

        StringBuilder answser = new StringBuilder();
        answser.append("\n");

        for (Node child : node.getChildren()) {
            answser.append("- ")
                    .append(child.getData().getNome())
                    .append(" - ").append(child.getData().getPath())
                    .append(" - ").append(child.getData().getDataCriacao())
                    .append(" - ").append(child.getData().getPermissoes())
                    .append("\n");
        }

        if (!node.getChildren().isEmpty()) {
            return answser.toString();
        }

        return "\n- Nothing is available to show \n";
    }

    /**
     * obtem a data de hoje e formata-a
     *
     * @return string com a data formatada
     */
    public String currentDateGoodFormat() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        return formatter.format(date);
    }

}
