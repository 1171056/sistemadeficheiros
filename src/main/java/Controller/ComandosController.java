package Controller;

import Model.SistemaTree.Node;
import Model.SistemaTree.Tree;
import Services.ElementoService;
import Services.FicheiroService;
import Services.PastaService;
import Utils.*;

public class ComandosController {

    private String inputUser;
    private Node currentNode;
    private Tree tree;
    private Utils utils;
    private PastaService pastaService;
    private FicheiroService ficheiroService;
    private ElementoService elementoService;
    private PermissoesEnum currentPermission;

    /**
     * Controller principal para as funcionalidades dos Comandos mencionados no *help*
     *
     * @param inputUser infomacao recebida pelo utilizador
     * @param root      elemento root da arvore do sistema
     */
    public ComandosController(String inputUser, Node root, Tree tree, PermissoesEnum permission) {
        this.inputUser = inputUser;
        this.currentNode = root;
        this.tree = tree;
        this.currentPermission = permission;
        this.pastaService = new PastaService(root, tree);
        this.ficheiroService = new FicheiroService(root, tree);
        this.elementoService = new ElementoService(root, tree);
        this.utils = new Utils();
    }

    /**
     * Cria uma pasta atraves do nome dado pelo utilizador
     *
     * @return feedback do sucesso/insucesso da operacao
     */
    public String criarPasta() {
        String[] inputs = inputUser.split(" ");
        String path = currentNode.getData().getPath() + "/" + inputs[1].trim();
        if (!tree.doesFolderPathExist(path)) {
            if (pastaService.criarPasta(path, inputs)) {
                return "\n- Folder created \n";
            }
        }
        return "\n- ERROR: something went wrong or incorrect command\n";
    }

    /**
     * Cria uma ficheiro atraves do nome dado pelo utilizador
     *
     * @return feedback do sucesso/insucesso da operacao
     */
    public String criarFicheiro() {
        String[] inputs = inputUser.split(" ");
        String path = currentNode.getData().getPath() + "/" + inputs[1].trim();
        if (!tree.doesFilePathExist(path)) {
            if (ficheiroService.criarFicheiro(path, inputs)) {
                return "\n- File created \n";
            }
        }
        return "\n- ERROR: something went wrong or incorrect command\n";
    }

    /**
     * Obtem todas as pastas e ficheiros criados
     *
     * @return feedback do sucesso/insucesso da operacao
     */
    public String obterElementos() {
        String[] inputs = inputUser.split(" ");
        return elementoService.obterElementos(inputs);
    }

    /**
     * Modifica o path em que o utilizador se encontra
     *
     * @return path para o qual o user foi movido
     */
    public Node modificarPathAtual() {
        String[] inputs = inputUser.split(" ");
        return elementoService.modificarPathAtual(inputs);
    }

    /**
     * Copia um ficheiro/pasta para a pasta pretendida
     *
     * @return feedback do sucesso/insucesso da operacao
     */
    public String copiarElemento() {
        String[] inputs = inputUser.split(" ");
        if (inputs.length == 3) {
            if (inputs[1].equalsIgnoreCase(inputs[2])) {
                return "\n- ERROR: first and second paths are the same\n";
            } else if (!tree.doesPathExist(inputs[2]) || !tree.doesPathExist(inputs[1])) {
                return "\n- ERROR: the path(s) dont exist\n";
            }
            if (elementoService.copiarElemento(inputs)) {
                return "\n- Copied with success\n";
            }
        }
        return "\n- ERROR: something went wrong or incorrect command\n";
    }

    /**
     * Mover um ficheiro/pasta para a pasta pretendida
     *
     * @return feedback do sucesso/insucesso da operacao
     */
    public String moverElemento() {
        String[] inputs = inputUser.split(" ");
        if (inputs.length == 3) {
            if (inputs[1].equalsIgnoreCase(inputs[2])) {
                return "\n- ERROR: first and second paths are the same\n";
            } else if (!tree.doesPathExist(inputs[2]) || !tree.doesPathExist(inputs[1])) {
                return "\n- ERROR: the path(s) dont exist\n";
            }
            if (elementoService.moverElemento(inputs)) {
                return "\n- Moved with success\n";
            }
        }
        return "\n- ERROR: something went wrong or incorrect command\n";
    }

    /**
     * Copia um ficheiro/pasta do sistema
     *
     * @return feedback do sucesso/insucesso da operacao
     */
    public String removerElemento() {
        String[] inputs = inputUser.split(" ");
        if (elementoService.removerElemento(inputs)) {
            return "\n- Removed with success\n";
        }
        return "\n- ERROR: something went wrong or incorrect command\n";
    }

    /**
     * Abre o ficheiro para ser editado
     *
     * @return feedback do sucesso/insucesso da operacao
     */
    public String editarFicheiro() {
        String[] inputs = inputUser.split(" ");
        if (ficheiroService.editarFicheiro(inputs, currentPermission)) {
            return "\n- Edited with success\n";
        }
        return "\n- ERROR: something went wrong or incorrect command\n";
    }

    /**
     * Abre o ficheiro e mostra o corpo do ficheiro
     *
     * @return corpo do ficheiro
     */
    public String abrirFicheiro() {
        String[] inputs = inputUser.split(" ");
        return ficheiroService.abrirFicheiro(inputs);
    }
}
