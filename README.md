# Sistema De Ficheiros

Sistema de Ficheiros estilo Unix - Command based file system

### Comandos

    - Criar pasta : mkdir *name*
    - Create file : touch *name* *?permissions?*
    - List folders and files on the current path : ls
    - Change path : cd *path*
    - Remove element from the system : rm *path*
    - Move element : mv *original path* *destination path*
    - Copy element : cp *original path* *destination path*
    - Edit a file : edit *file path*
    - Open a file : cat *file path*
    - Change to Admin : sudo
    - Change to normal user : unsudo
    
Nota : todos os paths devem ser escritos por completos (eg. "cd root/pasta/pasta2" )